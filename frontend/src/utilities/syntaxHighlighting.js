export const syntaxHighlighting = (code, language) => {
  let patterns = {
    'javascript': {
      'keywords': /\b(abstract|boolean|break|byte|case|catch|char|class|const|continue|debugger|default|delete|do|double|else|enum|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|let|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with|yield)\b/g,
      'strings': /('(?:\\.|[^\\'])*')|("(?:\\.|[^\\\"])*")/g,
      'comments': /(\/\*[\s\S]*?\*\/)|(\/\/.*$)/gm
    },
    'python': {
      'keywords': /\b(False|None|True|and|as|assert|async|await|break|class|continue|def|del|elif|else|except|finally|for|from|global|if|import|in|is|lambda|let|nonlocal|not|or|pass|raise|return|try|while|with|yield)\b/g,
      'strings': /('(?:\\.|[^\\'])*')|("(?:\\.|[^\\\"])*")/g,
      'comments': /(#.*$)/gm
    }
  };
  let result = [];
  let match;
  let langPatterns = patterns[language] || {};
  Object.keys(langPatterns).forEach((type) => {
    while ((match = langPatterns[type].exec(code)) !== null) {
      result.push(`<span class=\"${type}\">${match[0]}</span>`);
    }
  });
  return result.join('');
};