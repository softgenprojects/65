import React from 'react';
import { Box, Input, Button } from '@chakra-ui/react';
import { useTerminal } from '@hooks/useTerminal.js';

export const TerminalComponent = () => {
  const { state, setInputValue, executeCommand } = useTerminal();

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleSubmit = () => {
    executeCommand(state.inputValue);
    setInputValue('');
  };

  return (
    <Box p={4} bg='gray.700' color='white' borderRadius='md'>
      <Box mb={4}>
        {state.output && <Box>{state.output}</Box>}
        {state.history.map((command, index) => (
          <Box key={index}>{command}</Box>
        ))}
      </Box>
      <Input
        value={state.inputValue}
        onChange={handleInputChange}
        placeholder='Enter command'
        bg='gray.800'
        mb={2}
      />
      <Button onClick={handleSubmit} colorScheme='blue'>Execute</Button>
    </Box>
  );
};