import { Box, Button, Input, Checkbox, Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton, useDisclosure, FormControl, FormLabel, Select } from '@chakra-ui/react';
import { useState } from 'react';
import { usePlanner } from '@hooks/usePlanner';

export const PlannerComponent = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { tasks, addTask, removeTask, updateTask, filterTasksByProject, navigateMonth, selectedDate, projects } = usePlanner();
  const [selectedProject, setSelectedProject] = useState('');

  const handleAddTask = (task) => {
    addTask(task);
    onClose();
  };

  return (
    <Box display={{ base: 'block', md: 'flex' }}>
      <Box flex='1'>
        {/* Sidebar for projects/categories */}
        <Box as='aside' p='4'>
          {projects.map((project) => (
            <Button key={project.id} onClick={() => setSelectedProject(project.id)} mb='2'>
              {project.name}
            </Button>
          ))}
        </Box>
      </Box>
      <Box flex='3'>
        {/* Calendar and tasks view */}
      </Box>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add a new task</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>Task name</FormLabel>
              <Input placeholder='Task name' />
            </FormControl>
            <FormControl mt='4'>
              <FormLabel>Due date</FormLabel>
              <Input placeholder='Due date' type='date' />
            </FormControl>
            <FormControl mt='4'>
              <FormLabel>Priority</FormLabel>
              <Select placeholder='Select priority'>
                <option>High</option>
                <option>Medium</option>
                <option>Low</option>
              </Select>
            </FormControl>
            <FormControl mt='4'>
              <FormLabel>Project</FormLabel>
              <Select placeholder='Select project'>
                {projects.map((project) => (
                  <option key={project.id} value={project.id}>{project.name}</option>
                ))}
              </Select>
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme='blue' mr='3' onClick={handleAddTask}>
              Add Task
            </Button>
            <Button variant='ghost' onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
};