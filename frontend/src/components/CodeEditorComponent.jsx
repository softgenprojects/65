import { Box, Flex, IconButton, useDisclosure, Button } from '@chakra-ui/react';
import { CloseIcon, AddIcon } from '@chakra-ui/icons';
import { useCodeEditor } from '@hooks/useCodeEditor.js';

export const CodeEditorComponent = () => {
  const { files, addFile, selectFile, removeFile, updateFileContent, activeFileId } = useCodeEditor();

  const handleAddFile = () => {
    const fileName = prompt('Enter the file name');
    if (fileName) {
      addFile(fileName);
    }
  };

  const activeFile = files.find(file => file.id === activeFileId);

  return (
    <Flex direction='column' h='100vh'>
      <Flex bg='gray.700' p={2} color='white'>
        {files.map(file => (
          <Flex key={file.id} align='center' mr={2} bg={activeFileId === file.id ? 'blue.500' : 'gray.600'} borderRadius='md' p={1}>
            <Box as='button' onClick={() => selectFile(file.id)}>{file.name}</Box>
            <IconButton aria-label='Close file' icon={<CloseIcon />} size='xs' onClick={() => removeFile(file.id)} />
          </Flex>
        ))}
        <IconButton aria-label='Add new file' icon={<AddIcon />} onClick={handleAddFile} />
      </Flex>
      <Box flex='1' p={4} bg='gray.100' color='black' overflowY='auto'>
        {activeFile && activeFile.content.map((piece, index) => (
          <span key={index} style={{ color: piece.type === 'keyword' ? 'blue' : piece.type === 'string' ? 'green' : piece.type === 'comment' ? 'grey' : 'black' }}>{piece.content}</span>
        ))}
      </Box>
    </Flex>
  );
};