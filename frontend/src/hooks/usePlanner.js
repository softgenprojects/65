import { useState, useEffect } from 'react';
import { fetchTasks, fetchProjects } from '@api/taskAPI';

export const usePlanner = () => {
  const [tasks, setTasks] = useState([]);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [projects, setProjects] = useState([]);

  const addTask = (task) => {
    setTasks([...tasks, task]);
  };

  const removeTask = (taskId) => {
    setTasks(tasks.filter(task => task.id !== taskId));
  };

  const updateTask = (updatedTask) => {
    setTasks(tasks.map(task => task.id === updatedTask.id ? updatedTask : task));
  };

  const filterTasksByProject = (projectId) => {
    return tasks.filter(task => task.projectId === projectId);
  };

  const navigateMonth = (direction) => {
    const currentMonth = selectedDate.getMonth();
    setSelectedDate(new Date(selectedDate.setMonth(currentMonth + direction)));
  };

  useEffect(() => {
    const init = async () => {
      const initialTasks = await fetchTasks();
      const initialProjects = await fetchProjects();
      setTasks(initialTasks);
      setProjects(initialProjects);
    };
    init();
  }, []);

  return {
    tasks,
    selectedDate,
    projects,
    addTask,
    removeTask,
    updateTask,
    filterTasksByProject,
    navigateMonth
  };
};