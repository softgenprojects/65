import { useReducer } from 'react';

export const useTerminal = () => {
  const initialState = {
    inputValue: '',
    output: '',
    history: []
  };

  const terminalReducer = (state, action) => {
    switch (action.type) {
      case 'SET_INPUT_VALUE':
        return { ...state, inputValue: action.payload };
      case 'ADD_TO_HISTORY':
        return { ...state, history: [...state.history, action.payload] };
      case 'SET_OUTPUT':
        return { ...state, output: action.payload };
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(terminalReducer, initialState);

  const setInputValue = (value) => {
    dispatch({ type: 'SET_INPUT_VALUE', payload: value });
  };

  const addToHistory = (command) => {
    dispatch({ type: 'ADD_TO_HISTORY', payload: command });
  };

  const setOutput = (output) => {
    dispatch({ type: 'SET_OUTPUT', payload: output });
  };

  const executeCommand = (command) => {
    // Logic to execute command
    // This is a placeholder for command execution logic
    const output = `Executed: ${command}`;
    setOutput(output);
    addToHistory(command);
  };

  return {
    state,
    setInputValue,
    executeCommand
  };
};