import { useState } from 'react';
import { syntaxHighlighting } from '@utilities/syntaxHighlighting.js';

export const useCodeEditor = () => {
  const [files, setFiles] = useState([]);
  const [activeFileId, setActiveFileId] = useState(null);

  const addFile = (fileName) => {
    const newFile = {
      id: Date.now(),
      name: fileName,
      content: ''
    };
    setFiles([...files, newFile]);
  };

  const selectFile = (fileId) => {
    setActiveFileId(fileId);
  };

  const removeFile = (fileId) => {
    setFiles(files.filter(file => file.id !== fileId));
  };

  const updateFileContent = (newContent) => {
    setFiles(files.map(file => {
      if (file.id === activeFileId) {
        return { ...file, content: syntaxHighlighting(newContent, file.language || 'javascript') };
      }
      return file;
    }));
  };

  return {
    files,
    addFile,
    selectFile,
    removeFile,
    updateFileContent,
    activeFileId
  };
};