import { Flex, Box, Text, Button, Image, Link } from '@chakra-ui/react';
import { FaArrowRight } from 'react-icons/fa';

const EditorPage = () => {
  return (
    <Flex direction='column' align='center' justify='center' h='100vh' bgGradient='linear(to-r, teal.500, green.500)'>
      <Box textAlign='center' color='white'>
        <Text fontSize={{ base: '24px', md: '40px', lg: '56px' }} fontWeight='bold'>Welcome to Joe Ros's World</Text>
        <Text mt={4} fontSize={{ base: '16px', md: '20px', lg: '24px' }}>Discover the beauty of design and code combined</Text>
        <Button mt={10} colorScheme='blue' rightIcon={<FaArrowRight />}>
          Explore Now
        </Button>
      </Box>
      <Box mt={10}>
        <Link href='https://joe-ros.com' isExternal>
          <Image borderRadius='full' boxSize='150px' src='https://i.pravatar.cc/150' alt='Joe Ros' />
        </Link>
      </Box>
    </Flex>
  );
};

export default EditorPage;